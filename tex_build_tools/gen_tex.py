#!/usr/bin/env python3

import json
from typing import Dict, Callable
from os.path import join as join_path, basename, abspath, dirname
from jinja2 import Environment, FileSystemLoader, select_autoescape

from .note_parser import parse_note
from .index import iterate_notes

env = Environment(
    loader=FileSystemLoader(searchpath=join_path(dirname(abspath(__file__)), "..")),
    autoescape=select_autoescape(["html", "xml"]),
    variable_start_string="\\VAR{",
    variable_end_string="}",
    block_start_string="\\BLOCK{",
    block_end_string="}",
)


def write_html_tex(
    note: str, args: Dict, cards: Dict, outdir: str, card_template_name_html: Callable
):
    for cn, c in cards.items():
        with open(join_path(outdir, f"{note}_{cn}.tex"), "w") as f:
            f.write(
                env.get_template(
                    join_path("notes", card_template_name_html(args, c))
                ).render(note=note, args=args, card=c)
            )
    json.dump({"args": args}, open(join_path(outdir, f"{note}.json"), "w"))


def gen_html_tex(args, card_template_name_html: Callable):
    s = args.note.read()
    write_html_tex(
        basename(args.note.name)[:-4],
        *parse_note(s),
        args.outdir,
        card_template_name_html,
    )


def gen_pdf_tex(args, tag_template_name_pdf: Callable):
    notes = {}
    for tag, name in iterate_notes(args.tag):
        with open(join_path("notes", tag, name + ".tex")) as f:
            a, cards = parse_note(f.read())
            notes[tag + "/" + name] = {"args": a, "cards": cards}
    with open(join_path(args.outdir, f"{args.tag}.tex"), "w") as f:
        f.write(
            env.get_template(
                join_path("notes", tag_template_name_pdf(args.tag))
            ).render(notes=notes)
        )
