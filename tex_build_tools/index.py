#!/usr/bin/env python3

import glob
from os.path import join, basename, split as split_path, sep


def iterate_notes(tag=None):
    dirs = (
        glob.glob(join("notes", "*") + sep)
        if tag is None
        else [join("notes", tag) + sep]
    )
    for d in dirs:
        _, tag = split_path(d[:-1])
        for tex in glob.glob(join(d, "*.tex")):
            name = basename(tex)[:-4]
            yield tag, name

