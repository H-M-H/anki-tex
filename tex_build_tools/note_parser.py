from typing import Tuple, Dict

import re

HEADER = re.compile(r"^%=(?P<header>.+)")
BODY = re.compile(r"%%(.+?)\n((?:.(?!%%))+)", re.DOTALL)


def parse_head(s: str) -> Dict:
    args = {}
    if (header := re.match(HEADER, s)) :
        args = {
            a[0]: a[1]
            for arg in header.group(1).strip().split()
            if (a := arg.split("="))
        }
    return args


def parse_note(s: str) -> Tuple[Dict, Dict]:
    cards = {
        h[0]: {
            "args": {a[0]: a[1] for arg in h[1:] if (a := arg.split("="))},
            "tex": m[1],
        }
        for m in re.findall(BODY, s)
        if (h := m[0].strip().split())
    }
    return parse_head(s), cards
