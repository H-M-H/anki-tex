#!/usr/bin/env python3

import os
import sys
from shutil import copyfile
from bs4 import BeautifulSoup


def add_prefix(fn):
    return "anki-tex_" + os.path.basename(os.path.abspath(os.path.curdir)) + "_" + fn


if len(sys.argv) != 2:
    print("USAGE: some.html")
    exit(1)

fn = os.path.basename(sys.argv[1])
os.chdir(os.path.dirname(sys.argv[1]))

with open(fn) as f:
    html = f.read()
    b = BeautifulSoup(html, "html.parser")

    for img in b.findAll("img"):
        if not img["src"].startswith("anki-tex"):
            new_fn = add_prefix(img["src"])
            os.rename(img["src"], new_fn)
            copyfile(new_fn, os.path.join("..", "..", "media", new_fn))
            img["src"] = new_fn

with open(fn, "w") as f:
    f.write(str(b))
