TEX=latexmk -lualatex -halt-on-error
HT4=make4ht --utf8 --format html5+mathjaxnode --build-file ../../../config.mk4 --config ../../../html.cfg --lua
NOTES=$(wildcard notes/*/*.tex)
HTMLS=$(shell ./index.py HTML)
CARDS=$(shell ./index.py CARD)
TAGS=$(patsubst notes/%/,%,$(dir $(wildcard notes/*/.)))
PDFS=$(patsubst %,build/pdf/%.pdf,$(TAGS))

.SECONDARY:

define pdf_tex_rule

build/pdf/$(strip $(1)).tex: gen_tex.py build_dir $(wildcard notes/$(strip $(1))/*.tex)
	./gen_tex.py pdf $(1) $$(dir $$@)

endef

define html_tex_rule

build/html/%_$(strip $(1)).tex: notes/%.tex gen_tex.py build_dir
	./gen_tex.py html $$< $$(dir $$@)

endef

all: pdf anki

pdf: $(PDFS)

clean:
	rm -rf build

build/empty:
	mkdir -p build/pdf $(addprefix build/html/,$(TAGS)) build/media
	touch build/empty

build_dir: build/empty

$(foreach _CARD, $(CARDS), $(eval $(call html_tex_rule, $(_CARD))))
$(foreach _TAG, $(TAGS), $(eval $(call pdf_tex_rule, $(_TAG))))

build/pdf/%.pdf: build/pdf/%.tex
	$(TEX) -outdir=$(dir $@) $< && touch $@

build/html/%.html: build/html/%.tex
	test -s $< || touch $@
	test -s $< && (cd $(dir $<) && $(HT4) $(notdir $<) -halt-on-error)
	test -s $< && ./rewrite_media.py $@

anki: build/anki.csv build_dir

build/anki.csv: anki.py $(HTMLS)
	./anki.py
