#!/usr/bin/env python3

import os.path as pt
import csv
import glob
import json

from notes.meta_data import note_types, cards

from tex_build_tools.note_parser import parse_head

csv_writers = {
    note_type: csv.writer(
        open(f"build/anki_{note_type}.csv", "w", newline=""), delimiter=";"
    )
    for note_type in set((*note_types(), "default"))
}

for d in glob.glob("notes/*/"):
    _, tag = pt.split(d[:-1])
    for tex in glob.glob(pt.join(d, "*.tex")):
        name = pt.basename(tex)[:-4]
        args = json.load(open(pt.join("build", "html", tag, f"{name}.json")))["args"]
        if not (note_type := args.get("type")):
            note_type = "default"
        anki_cards = []
        for c in cards(note_type):
            fn = pt.join("build", "html", tag, f"{name}_{c}.html")
            with open(fn) as f:
                anki_cards.append(f.read())
        if (tags := args.get("tags")) :
            anki_tags = " ".join(sorted(set([tag, *tags.split(",")])))
        else:
            anki_tags = tag
        csv_writers[note_type].writerow([tag + "/" + name, *anki_cards, anki_tags])
