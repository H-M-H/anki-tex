#!/usr/bin/env python3

from tex_build_tools.gen_tex import gen_html_tex, gen_pdf_tex
from notes.meta_data import card_template_name_html, tag_template_name_pdf

import argparse

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()

parser_html = subparsers.add_parser("html", help="html help")
parser_html.add_argument("note", type=argparse.FileType("r"))
parser_html.add_argument("outdir", type=str)
parser_html.set_defaults(func=gen_html_tex, template_func=card_template_name_html)

parser_pdf = subparsers.add_parser("pdf", help="pdf help")
parser_pdf.add_argument("tag", type=str)
parser_pdf.add_argument("outdir", type=str)
parser_pdf.set_defaults(func=gen_pdf_tex, template_func=tag_template_name_pdf)

args = parser.parse_args()
args.func(args, args.template_func)
