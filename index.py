#!/usr/bin/env python3

import sys
from os.path import join

from tex_build_tools.index import iterate_notes
from tex_build_tools.note_parser import parse_head
from notes.meta_data import note_types, cards

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"USAGE: {sys.argv[0]} HTML|CARD")
        exit(1)

    if sys.argv[1] == "CARD":
        for c in set(c for note_type in (*note_types(), "default") for c in cards(note_type)):
            print(c)
        exit()

    for tag, name in iterate_notes():
        with open(join("notes", tag, name + ".tex")) as f:
            args = parse_head(f.readline())
        if not (note_type := args.get("type")):
            note_type = "default"
        for cn in cards(note_type):
            print(join("build", "html", tag, f"{name}_{cn}.html"))
